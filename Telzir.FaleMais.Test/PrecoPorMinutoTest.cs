﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telzir.FaleMais.Models;

namespace Telzir.FaleMais.Test
{
    [TestClass]
    public class PrecoPorMinutoTest
    {
        [TestMethod]
        public void Calcular_o_valor_por_minuto()
        {
            var preco = new PrecoPorMinuto { Id = 1, DDD_Origem = "011", DDD_Destino = "016", ValorPorMinuto = 1.90m };
            var retorno = preco.CalcularValorPorMinuto(15);
            Assert.AreEqual(28.50m, retorno);
        }
    }
}
