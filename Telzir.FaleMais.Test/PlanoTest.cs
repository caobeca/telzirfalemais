﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telzir.FaleMais.Models;
namespace Telzir.FaleMais.Test
{
    [TestClass]
    public class PlanoTest
    {
        [TestMethod]
        public void Calculo_de_taxa_com_minutos_excedentes_utilizando_10_porcento_como_base()
        {
            var plano = new Plano{Id = 1, Descricao="FaleMais 30",Minutos=30, TaxaDeMinutosExcedentes= 0.1m };
            var retorno = plano.AcrescimoDosMinutosExcedentes(100);
            Assert.AreEqual(110, retorno);
        }
    }
}
