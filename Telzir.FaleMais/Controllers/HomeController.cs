﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telzir.FaleMais.Models;
using Telzir.FaleMais.Repositories;

namespace Telzir.FaleMais.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var planoRepo = new PlanoRepository();
            var precoRepo = new PrecoPorMinutoRepository();
            var model = new HomeViewModel();

            model.Precos = precoRepo.ListarPrecos();
            model.Planos = planoRepo.ListarPlanos();

            return View(model);
        }

    }
}