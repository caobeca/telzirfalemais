﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telzir.FaleMais.Models;
using Telzir.FaleMais.Repositories;

namespace Telzir.FaleMais.Controllers
{
    public class CalcularPrecoController : Controller
    {
        public ActionResult Index()
        {
            var planoRepo = new PlanoRepository();
            var model = new CalcularPrecoViewModel();
            planoRepo.ListarPlanos().ToList().ForEach(el => model.Planos.Add(new SelectListItem()
            {
                Text = el.Descricao,
                Value = el.Id.ToString()
            }));
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(CalcularPrecoViewModel model)
        {
            var planoRepo = new PlanoRepository();
            planoRepo.ListarPlanos().ToList().ForEach(el => model.Planos.Add(new SelectListItem()
            {
                Text = el.Descricao,
                Value = el.Id.ToString()
            }));
            return View(model);
       }


	}
}