﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Telzir.FaleMais.Models
{
    public class HomeViewModel
    {
        public List<Plano> Planos { get; set; }

        public List<PrecoPorMinuto> Precos { get; set; }


    }
}