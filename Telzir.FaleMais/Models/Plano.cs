﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Telzir.FaleMais.Models
{
    public class Plano
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(25)]
        public string Descricao { get; set; }

        [Required]
        public int Minutos { get; set; }

        [Required]
        public decimal TaxaDeMinutosExcedentes { get; set; }

        public decimal AcrescimoDosMinutosExcedentes(decimal valor) { 
            return valor + ( this.TaxaDeMinutosExcedentes * valor);
        }

    }
}