﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Telzir.FaleMais.Models
{
    public class PrecoPorMinuto
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }
        [Key]
        [Column(Order = 1)]
        [MaxLength(3)]
        [MinLength(3)]
        public string DDD_Origem { get; set; }
        [Key]
        [MaxLength(3)]
        [MinLength(3)]
        [Column(Order = 2)]
        public string DDD_Destino { get; set; }
        public decimal ValorPorMinuto { get; set; }

        public decimal CalcularValorPorMinuto(int min)
        {
            return this.ValorPorMinuto * min;
        }
    }
}