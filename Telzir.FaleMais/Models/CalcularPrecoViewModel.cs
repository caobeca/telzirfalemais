﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telzir.FaleMais.Repositories;

namespace Telzir.FaleMais.Models
{
    public class CalcularPrecoViewModel
    {

        public List<SelectListItem> Planos { get; set; }

        [Range(1, 99999999999)]        
        public int Minuto { get; set; }
        
        [Required]
        [Range(1, 999)]
        [MaxLength(3)]
        [MinLength(3)]
        public string Origem { get; set; }
        
        [Required]
        [Range(1, 999)]
        [MaxLength(3)]
        [MinLength(3)]
        public string Destino { get; set; }

        public PrecoPorMinuto Preco
        {
            get
            {
                var repo = new PrecoPorMinutoRepository();
                var obj = repo.ListarPrecos().FirstOrDefault(p => p.DDD_Origem == Origem && p.DDD_Destino == Destino);
                return obj == null ? new PrecoPorMinuto() : obj;
            }
        }
        
        public Plano Plano
        {
            get
            {
                var repo = new PlanoRepository();
                var obj = Planos.FirstOrDefault(p => p.Selected);
                return obj != null ? (repo.BuscarPeloIdentificador(Int32.Parse(obj.Value))) : new Plano();
            }
        }

        public bool IsValid
        {
            get
            {
                return Preco.Id != 0 && Minuto > 0;
            }
        }

        public decimal ValorSemPlano
        {
            get
            {
                return Preco.CalcularValorPorMinuto(Minuto);
            }
        }

        public decimal ValorComPlano
        {
            get
            {
                if (Plano.Minutos > Minuto)
                {
                    return 0;
                }
                else
                {
                    var min =  Minuto - Plano.Minutos;
                    return Plano.AcrescimoDosMinutosExcedentes(Preco.CalcularValorPorMinuto(min));
                }
            }
        }
        
        public CalcularPrecoViewModel()
        {
            this.Planos = new List<SelectListItem>();
        }

        
    }
}