﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telzir.FaleMais.DataContexts;
using Telzir.FaleMais.Models;

namespace Telzir.FaleMais.Repositories
{
    public class PrecoPorMinutoRepository
    {
        private MyContext _MyContext { get; set; } 
        public PrecoPorMinutoRepository()
        {
            this._MyContext = new MyContext();
        }

        public List<PrecoPorMinuto> ListarPrecos()
        {
            return (from PrecoPorMinuto p in this._MyContext.Precos orderby p.Id select p)
                .ToList();
        }

        public PrecoPorMinuto BuscarPeloIdentificador(int id)
        {
            return (from PrecoPorMinuto p in this._MyContext.Planos where p.Id == id select p)
                    .FirstOrDefault();
        }
    }
}