﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telzir.FaleMais.DataContexts;
using Telzir.FaleMais.Models;

namespace Telzir.FaleMais.Repositories
{
    public class PlanoRepository
    {
        private MyContext _MyContext { get; set; }

        public PlanoRepository()
        {
            this._MyContext = new MyContext();
        }

        public List<Plano> ListarPlanos() 
        {
            return (from Plano p in this._MyContext.Planos orderby p.Id select p)
                .ToList();
        }

        public Plano BuscarPeloIdentificador(int id)
        {
            return (from Plano p in this._MyContext.Planos where p.Id == id select p)
                    .FirstOrDefault();
        }

    }
}