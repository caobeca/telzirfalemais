﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Telzir.FaleMais.Models;

namespace Telzir.FaleMais.DataContexts
{
    public class MyContext : DbContext
    {
        public MyContext()
            : base("DefaultConnection")
        {
            //A linha abaixo deve ser comenta quando o site for para produção
            //Serve para criar uma base de dados limpa apenas com os registros do Seed 
            Database.SetInitializer<MyContext>(new Initializer());   
        }
        public DbSet<Plano> Planos { get; set; }
        public DbSet<PrecoPorMinuto> Precos { get; set; }

        public class Initializer : DropCreateDatabaseAlways<MyContext>
        {
            protected override void Seed(MyContext context)
            {
                context.Planos.Add(new Plano { Id = 1, Descricao="FaleMais 30",Minutos=30, TaxaDeMinutosExcedentes= 0.1m });
                context.Planos.Add(new Plano { Id = 2, Descricao = "FaleMais 60", Minutos = 60, TaxaDeMinutosExcedentes = 0.1m });
                context.Planos.Add(new Plano { Id = 3, Descricao = "FaleMais 120", Minutos = 120, TaxaDeMinutosExcedentes = 0.1m });

                context.Precos.Add(new PrecoPorMinuto { Id=1, DDD_Origem="011",DDD_Destino="016",ValorPorMinuto = 1.90m });
                context.Precos.Add(new PrecoPorMinuto { Id = 2, DDD_Origem = "016", DDD_Destino = "011", ValorPorMinuto = 2.90m });
                context.Precos.Add(new PrecoPorMinuto { Id = 3, DDD_Origem = "011", DDD_Destino = "017", ValorPorMinuto = 1.70m });
                context.Precos.Add(new PrecoPorMinuto { Id = 4, DDD_Origem = "017", DDD_Destino = "011", ValorPorMinuto = 2.70m });
                context.Precos.Add(new PrecoPorMinuto { Id = 5, DDD_Origem = "011", DDD_Destino = "018", ValorPorMinuto = 0.90m });
                context.Precos.Add(new PrecoPorMinuto { Id = 6, DDD_Origem = "018", DDD_Destino = "011", ValorPorMinuto = 1.90m });
                context.SaveChanges();
                base.Seed(context);
            }
        }

    }
}